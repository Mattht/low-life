﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;


namespace Low_Life
{
    class Pedestrian
    {
        public string color;
        public bool alive;

        public Body pedBody;
        public Geom pedGeom;
        public Texture2D pedTexture;
        public Vector2 pedOrigin;

        public bool isRanOver = false;
        public Vector2 deathPosition;
        public float carRotation; // Rotation of the car that hits the enemy
        
        public float minPedVelocity = 40;
        public float maxPedVelocity = 80;

        public const int maxEnemies = 30;
        public const float maxEnemyHeight = 0.04f;
        public const float minEnemyHeight = 0.9f;
        public const float maxEnemyWidth = 0.04f;
        public const float minEnemyWidth = 0.9f;
        public const float maxEnemyVelocity = 3.0f;
        public const float minEnemyVelocity = 1.0f;


        public Pedestrian(Texture2D loadedtexture)
        {
            pedTexture = loadedtexture;

            pedBody = BodyFactory.Instance.CreateRectangleBody(this.pedTexture.Height, this.pedTexture.Width, 1);
            pedOrigin = new Vector2(this.pedTexture.Width / 2, this.pedTexture.Height / 2);
            pedBody.Mass = 0.1f;

            Vertices pedVertices = new Vertices(); // Anti Clock for some reason:
            pedVertices.Add(new Vector2(-1, -2));
            pedVertices.Add(new Vector2(1, -2));
            pedVertices.Add(new Vector2(1, 2));
            pedVertices.Add(new Vector2(-1, 2));

            pedGeom = new Geom(this.pedBody, pedVertices, 11);
            pedGeom.CollisionGroup = 1; // Same as walls
            pedGeom.RestitutionCoefficient = 0.0f;
            pedGeom.FrictionCoefficient = 0.0f;
            pedGeom.Tag = this;

        }

        public bool RanOver(Pedestrian mec, Car voiture)
        {
            //mec.pedBody.Position = new Vector2(500, 400);
            mec.carRotation = voiture.carBody.Rotation;
            mec.deathPosition = mec.pedBody.Position;
            mec.pedBody.ResetDynamics();
            mec.isRanOver = true;
            return true;
        }

    }
}

