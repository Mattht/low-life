﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace Low_Life
{
    class Box
    {
        public Body boxBody;
        public Geom boxGeom;
        public Texture2D boxTexture;
        public Vector2 boxOrigin;

        
        
        public Box(Texture2D loadedtexture)
        {
            boxTexture = loadedtexture;
            boxOrigin = new Vector2(this.boxTexture.Width / 2, this.boxTexture.Height / 2);

            boxBody = BodyFactory.Instance.CreateRectangleBody(this.boxTexture.Height, this.boxTexture.Width, 1);
            boxBody.Position = new Vector2(400, 320);
            boxBody.MomentOfInertia = 200;
            boxBody.Mass = 10;

            boxGeom = GeomFactory.Instance.CreateRectangleGeom(this.boxBody, this.boxTexture.Width, this.boxTexture.Height);
            boxGeom.RestitutionCoefficient = 0.3f;
            boxGeom.FrictionCoefficient = 0.5f;
        } 
    }
}
