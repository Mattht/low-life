﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace Low_Life
{
    class Car
    {
        //All that is needed to create and draw a rectangle
        public Body carBody;
        public Geom carGeom;
        public Texture2D carTexture;
        public Vector2 carOrigin;

        public bool crash;
        public int playCrash = 0;

        public int score;
        public string giveScore;
        public Vector2 positionScore;

        
        
        public Car(Texture2D loadedtexture)
        {
            
            carTexture = loadedtexture;
            carOrigin = new Vector2(this.carTexture.Width / 2, this.carTexture.Height / 2);

            carBody = BodyFactory.Instance.CreateRectangleBody(this.carTexture.Height, this.carTexture.Width, 1);
            carBody.LinearDragCoefficient = 1;
            carBody.RotationalDragCoefficient = 1;

            // Complex Geom:
            Vertices carVertices = new Vertices(); // Anti Clock for some reason:
            carVertices.Add(new Vector2(-36, -5));
            carVertices.Add(new Vector2(-35, -13));
            carVertices.Add(new Vector2(-34, -16));
            carVertices.Add(new Vector2(-33, -18));
            carVertices.Add(new Vector2(-31, -20)); // Gauche de la roue en bas a gauche (Car regardant la droite)
            carVertices.Add(new Vector2(-22, -20));
            carVertices.Add(new Vector2(-21, -19));
            // Bas Droit
            carVertices.Add(new Vector2(+13, -19));
            carVertices.Add(new Vector2(+14, -20));
            carVertices.Add(new Vector2(+29, -20));
            carVertices.Add(new Vector2(+32, -17));
            carVertices.Add(new Vector2(+34, -13));
            carVertices.Add(new Vector2(+35, -10));
            carVertices.Add(new Vector2(+36, -1));
            // Haut Droit
            carVertices.Add(new Vector2(+36, +1));
            carVertices.Add(new Vector2(+35, +10));
            carVertices.Add(new Vector2(+34, +13));
            carVertices.Add(new Vector2(+32, +17));
            carVertices.Add(new Vector2(+29, +20));
            carVertices.Add(new Vector2(+14, +20));
            carVertices.Add(new Vector2(+13, +19));
            // Haut Gauche
            carVertices.Add(new Vector2(-20, +19));
            carVertices.Add(new Vector2(-21, +20));
            carVertices.Add(new Vector2(-31, +20));
            carVertices.Add(new Vector2(-33, +18));
            carVertices.Add(new Vector2(-34, +16));
            carVertices.Add(new Vector2(-35, +13));
            carVertices.Add(new Vector2(-36, +5));

            carGeom = new Geom(this.carBody, carVertices, 11);
            carGeom.RestitutionCoefficient = 0.0f;
            carGeom.FrictionCoefficient = 0.0f;
            carGeom.Tag = this;
            carGeom.OnCollision += this.RunOver;
            carGeom.OnSeparation += this.NoBump;
            carGeom.OnCollision += this.Bump;

            score = 0;
            giveScore = score.ToString();
        }

        private bool RunOver(Geom geom1, Geom geom2, ContactList contactList)
        {
            Pedestrian mec = geom1.Tag as Pedestrian;// on tente de voir si la geom1 ou la geom2 ont pour Tag un mec qu'on peut écraser
            Car voiture = geom2.Tag as Car;
                        
            if (mec == null)
            {
                mec = geom2.Tag as Pedestrian;
                voiture = geom1.Tag as Car;
            }

            if (mec != null)
            {
                this.score += 1;
                this.giveScore = this.score.ToString();
                mec.RanOver(mec, voiture);
            }
            return true;
        }
        
        private bool Bump(Geom geom1, Geom geom2, ContactList contactList)
        {
            Car ourCar = geom1.Tag as Car;
            Car other = geom2.Tag as Car;
            if (ourCar == null || other == null)
            {
                Border limit = geom1.Tag as Border;

                if (limit == null)
                {
                    limit = geom2.Tag as Border;
                }
                if (limit != null && playCrash == 0)
                {
                    playCrash += 1;
                    crash = true;
                }
            }
            if (ourCar != null && other != null && playCrash == 0)
            {
                playCrash += 1;
                crash = true;
            }


            return true;
        }

        private void NoBump(Geom geom1, Geom geom2)
        {
            Car ourCar = geom1.Tag as Car;
            Car other = geom2.Tag as Car;
            if (ourCar == null || other == null)
            {
                Border limit = geom1.Tag as Border;

                if (limit == null)
                {
                    limit = geom2.Tag as Border;
                }
                if (limit != null)
                {
                    playCrash = 0;
                }
            }
            if (ourCar != null && other != null)
            {
                playCrash = 0;
            }
            
            /*Border limit = geom1.Tag as Border;

            if (limit == null)
            {
                limit = geom2.Tag as Border;
            }
            if (limit != null)
            {
                playCrash = 0;
            }*/ 
        }

    }
}