﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace Low_Life
{
    class Blood
    { 
        
        public Texture2D bloodTexture;
        public Vector2 position;
        public float rotation;
        public Vector2 center;
        public string color = "Yellow";

        public const int maxBlood = 200;

        public Blood(Texture2D loadedtexture)
        {
            bloodTexture = loadedtexture;
            rotation = 0.0f;
            position = new Vector2(-20, -20);
            center = new Vector2(bloodTexture.Width / 2, bloodTexture.Height / 2);
        }
        

    }
}
