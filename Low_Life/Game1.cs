using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

// Pourquoi la voiture tourne toute seul?


namespace Low_Life
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //Default
        GraphicsDeviceManager graphics;
        SpriteFont ScoreFont;
        SpriteBatch spriteBatch;

        AudioEngine audioEngine;
        WaveBank waveBank;
        SoundBank soundBank;
        
        // ----------------------------------
        //The physics simulator is always needed. We supply a gravity of 250 units
        PhysicsSimulator simulator = new PhysicsSimulator(new Vector2(0, 0));
        // ----------------------------------

        // BG
        Texture2D backgroundTexture;
        Rectangle viewportRect;
        
        // Borders
        Border right;
        Border left;
        Border top;
        Border bottom;

        // Boxes
        Box first;

        // Players
        Car player1;
        Car player2;
        Car player3;
        Car player4;

        // Blood
        Blood[] bloodOnFloor;
        Blood[] blood2OnFloor;
        Blood[] pedOnFloor;
        Blood[] pedbOnFloor;
        Blood[] ped2OnFloor;
        Blood[] pedb2OnFloor;
        Blood[] ped3OnFloor;
        Blood[] pedb3OnFloor;
        Blood[] ped4OnFloor;
        Blood[] pedb4OnFloor;
        Blood[] ped5OnFloor;
        Blood[] pedb5OnFloor;
        int instance = 0;
        int maxInstance = 1;
        int destroyInstance;

        // Bots
        Random random = new Random();
        Random path = new Random();
        int nextPath;
        Random body = new Random();
        int nextBody;
        Random sound = new Random();
        int pedTalk;
        bool gameStarted = false;
        Pedestrian[] enemies;
        


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            audioEngine = new AudioEngine("Content/Sounds/xactProject.xgs");
            waveBank = new WaveBank(audioEngine, "Content/Sounds/myWaveBank.xwb");
            soundBank = new SoundBank(audioEngine, "Content//Sounds/mySoundBank.xsb");

            
            // TODO: use this.Content to load your game content here

            backgroundTexture = Content.Load<Texture2D>("Sprites\\CarPark");

            ScoreFont = Content.Load<SpriteFont>("Fonts\\ScoreFont");

            // Borders

                    //Right
            right = new Border(Content.Load<Texture2D>("Sprites\\borderRight"));
            right.borderBody.Position = new Vector2(790, 300);
                simulator.Add(right.borderBody);
                simulator.Add(right.borderGeom);

                    //Left
            left = new Border(Content.Load<Texture2D>("Sprites\\borderRight"));
            left.borderBody.Position = new Vector2(10, 300);
                simulator.Add(left.borderBody);
                simulator.Add(left.borderGeom);

                    //Top
            top = new Border(Content.Load<Texture2D>("Sprites\\top"));
            top.borderBody.Position = new Vector2(400, 10);
                simulator.Add(top.borderBody);
                simulator.Add(top.borderGeom);

                    //Bottom
            bottom = new Border(Content.Load<Texture2D>("Sprites\\top"));
            bottom.borderBody.Position = new Vector2(400, 590);
                simulator.Add(bottom.borderBody);
                simulator.Add(bottom.borderGeom);


            // Box

            first = new Box(Content.Load<Texture2D>("Sprites\\Box"));
            
            simulator.Add(first.boxBody);
            simulator.Add(first.boxGeom);
            
            
            // Players

                // Player 1
            player1 = new Car(Content.Load<Texture2D>("Sprites\\Car51"));
            player1.positionScore = new Vector2(5, 0);

                    //Config the car body
                    player1.carBody.Position = new Vector2(60, 501);
                    simulator.Add(player1.carBody);

                // Player 2
            player2 = new Car(Content.Load<Texture2D>("Sprites\\Car52"));
            player2.positionScore = new Vector2(780, 0);

                    //Config the car body
                    player2.carBody.Position = new Vector2(740, 179);
                    player2.carBody.Rotation = 3.15f;
                    simulator.Add(player2.carBody);

                // Player 3
            player3 = new Car(Content.Load<Texture2D>("Sprites\\Car53"));
            player3.positionScore = new Vector2(5, 580);

                    //Config the car body
                    player3.carBody.Position = new Vector2(565, 390);
                    player3.carBody.Rotation = 3.15f;
                    simulator.Add(player3.carBody);

                // Player 4
            player4 = new Car(Content.Load<Texture2D>("Sprites\\Car54"));
            player4.positionScore = new Vector2(780, 580);

                    //Config the car body
                    player4.carBody.Position = new Vector2(235, 120);
                    simulator.Add(player4.carBody);
            
                // Add all the Geoms to the sim:
                    simulator.Add(player1.carGeom);
                    simulator.Add(player2.carGeom);
                    simulator.Add(player3.carGeom);
                    simulator.Add(player4.carGeom);

                
            // Bots
            enemies = new Pedestrian[Pedestrian.maxEnemies];
            for (int i = 0; i < Pedestrian.maxEnemies; i++)
            {
                if (i % 7 == 0)
                {
                    enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian")); //Yellow
                    enemies[i].color = "Yellow";
                }
                else if (i % 2 == 0 && i != 0)
                {
                    if (i % 3 == 0)
                    {
                        enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian4")); //Green
                        enemies[i].color = "Green";
                    }
                    else
                    {
                        enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian2")); //Blue
                        enemies[i].color = "Blue";
                    }
                }
                else if (i % 3 == 0 && i != 0)
                {
                    enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian3")); //Red
                    enemies[i].color = "Red";
                }
                else if (i % 5 == 0)
                {
                    enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian4")); //Green
                    enemies[i].color = "Green";
                }
                else
                {
                    enemies[i] = new Pedestrian(Content.Load<Texture2D>("Sprites\\Pedestrian5")); //Purple
                    enemies[i].color = "Purple";
                }
                simulator.Add(enemies[i].pedBody);
                simulator.Add(enemies[i].pedGeom);
            }

            // Blood
            bloodOnFloor = new Blood[Blood.maxBlood];
            blood2OnFloor = new Blood[Blood.maxBlood];
            pedOnFloor = new Blood[Blood.maxBlood];
            pedbOnFloor = new Blood[Blood.maxBlood];
            ped2OnFloor = new Blood[Blood.maxBlood];
            pedb2OnFloor = new Blood[Blood.maxBlood];
            ped3OnFloor = new Blood[Blood.maxBlood];
            pedb3OnFloor = new Blood[Blood.maxBlood];
            ped4OnFloor = new Blood[Blood.maxBlood];
            pedb4OnFloor = new Blood[Blood.maxBlood];
            ped5OnFloor = new Blood[Blood.maxBlood];
            pedb5OnFloor = new Blood[Blood.maxBlood];
            for (int i = 0; i < Blood.maxBlood; i++) // Creates the max number of blood
            {
                bloodOnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\blood"));
                blood2OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\blood2"));
                pedOnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPed"));
                pedbOnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPedb"));
                ped2OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPed2"));
                pedb2OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPedb2"));
                ped3OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPed3"));
                pedb3OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPedb3"));
                ped4OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPed4"));
                pedb4OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPedb4"));
                ped5OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPed5"));
                pedb5OnFloor[i] = new Blood(Content.Load<Texture2D>("Sprites\\DeadPedb5"));
            }

            viewportRect = new Rectangle(0, 0,
                graphics.GraphicsDevice.Viewport.Width,
                graphics.GraphicsDevice.Viewport.Height);
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            /*
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            player1.rotation += gamePadState.ThumbSticks.Left.X * 0.1f;
            */ 
#if !XBOX

            
            KeyboardState keyboardState = Keyboard.GetState();
            
            // Controling Player Geom Car
            const float speed = 2;
            Vector2 force = Vector2.Zero;
            Vector2 force2 = Vector2.Zero;
            Vector2 force3 = Vector2.Zero;
            float turn = player1.carBody.Rotation; // get rotation
            float turn2 = player2.carBody.Rotation; // get rotation
            float turn3 = player3.carBody.Rotation; // get rotation
            //Vector2 forceStillActive = player1.carBody.Force; // get force
            force.Y = -force.Y;
            force2.Y = -force2.Y;

            
            // Car Body I
            if (keyboardState.IsKeyDown(Keys.L)) { force -= new Vector2((float)Math.Cos(turn) * speed, (float)Math.Sin(turn) * speed); }
            if (keyboardState.IsKeyDown(Keys.K)) { player1.carBody.RotationalDragCoefficient = 0; turn -= 25; }
            if (keyboardState.IsKeyDown(Keys.O)) { force += new Vector2((float)Math.Cos(turn) * speed, (float)Math.Sin(turn) * speed); player1.carBody.LinearDragCoefficient = 0; }
            if (keyboardState.IsKeyDown(Keys.M)) { player1.carBody.RotationalDragCoefficient = 0; turn += 25; }
            if (keyboardState.IsKeyUp(Keys.K) && keyboardState.IsKeyUp(Keys.M)) { turn = 0; player1.carBody.RotationalDragCoefficient = 1000; } // TRICHE: La voiture tourne toutes seul dans une sens sans ceci -_-"
            if (keyboardState.IsKeyUp(Keys.L) && keyboardState.IsKeyUp(Keys.O)) { player1.carBody.LinearDragCoefficient = 2; }

            player1.carBody.ApplyImpulse(force);
            player1.carBody.ApplyAngularImpulse(turn);

            
            // Car  Body II
            if (keyboardState.IsKeyDown(Keys.S)) { force2 -= new Vector2((float)Math.Cos(turn2) * speed, (float)Math.Sin(turn2) * speed); }
            if (keyboardState.IsKeyDown(Keys.Q)) { turn2 -= 20; }
            if (keyboardState.IsKeyDown(Keys.Z)) { force2 += new Vector2((float)Math.Cos(turn2) * speed, (float)Math.Sin(turn2) * speed); }
            if (keyboardState.IsKeyDown(Keys.D)) { turn2 += 20; }
            if (keyboardState.IsKeyUp(Keys.Q) && keyboardState.IsKeyUp(Keys.D)) { turn2 = 0; } // TRICHE: La voiture tourne toutes seul dans une sens sans ceci -_-"

            player2.carBody.ApplyImpulse(force2);
            player2.carBody.ApplyAngularImpulse(turn2);
            /*
            // Car Body III
            if (keyboardState.IsKeyDown(Keys.Down)) { force3 -= new Vector2((float)Math.Cos(turn3) * speed, (float)Math.Sin(turn3) * speed); }
            if (keyboardState.IsKeyDown(Keys.Left)) { turn3 -= 20; }
            if (keyboardState.IsKeyDown(Keys.Up)) { force3 += new Vector2((float)Math.Cos(turn3) * speed, (float)Math.Sin(turn3) * speed); }
            if (keyboardState.IsKeyDown(Keys.Right)) { turn3 += 20; }
            if (keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Right)) { turn3 = 0; } // TRICHE: La voiture tourne toutes seul dans une sens sans ceci -_-"

            player3.carBody.ApplyImpulse(force3);
            player3.carBody.ApplyAngularImpulse(turn3);
            */
            // END




            /*
            // PLAYER 1 (Other technique)
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                //soundBank.PlayCue("SFX_COMPACT_ENGINE");
                player1.carBody.LinearVelocity.X += (float)Math.Cos(player1.carBody.Rotation) * 6.0f;
                player1.carBody.LinearVelocity.Y += (float)Math.Sin(player1.carBody.Rotation) * 6.0f;
                if (keyboardState.IsKeyDown(Keys.Left))
                {
                    player1.carBody.Rotation -= 0.03f;
                }
                if (keyboardState.IsKeyDown(Keys.Right))
                {
                    player1.carBody.Rotation += 0.03f;
                }
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {

                player1.carBody.LinearVelocity.X += (float)Math.Cos(player1.carBody.Rotation) * -3.0f;
                player1.carBody.LinearVelocity.Y += (float)Math.Sin(player1.carBody.Rotation) * -3.0f;
                if (keyboardState.IsKeyDown(Keys.Left))
                {
                    player1.carBody.Rotation -= 0.03f;
                }
                if (keyboardState.IsKeyDown(Keys.Right))
                {
                    player1.carBody.Rotation += 0.03f;
                }
            }
            */ 
            
#endif

            if (player1.crash && player1.playCrash == 1)
            {
                soundBank.PlayCue("SFX_COLLISION_CAR_CAR_HARD");
                player1.playCrash += 1;
            }
            if (player2.crash && player2.playCrash == 1)
            {
                soundBank.PlayCue("SFX_COLLISION_CAR_CAR_HARD");
                player2.playCrash += 1;
            }
            if (player3.crash && player3.playCrash == 1)
            {
                soundBank.PlayCue("SFX_COLLISION_CAR_CAR_HARD");
                player3.playCrash += 1;
            }
            if (player4.crash && player4.playCrash == 1)
            {
                soundBank.PlayCue("SFX_COLLISION_CAR_CAR_HARD");
                player4.playCrash += 1;
            }

            
            UpdateEnemies();

            audioEngine.Update();

            // TODO: Add your update logic here

            //We update the physics simulator
            simulator.Update(gameTime.ElapsedGameTime.Milliseconds * .001f);

            base.Update(gameTime);
        }

        

        // PEDESTRIAN WALK
        public void UpdateEnemies()
        {
            
            foreach(Pedestrian enemy in enemies)
            {
                if (enemy.alive)
                {
                    if (enemy.isRanOver == true) // Death by RanOver
                    {
                        nextBody = body.Next(2);
                        if (enemy.color == "Yellow")
                        {
                            if (nextBody == 0)
                            {
                                pedOnFloor[instance].position = enemy.deathPosition;
                                pedOnFloor[instance].rotation = enemy.carRotation;
                            }
                            if (nextBody == 1)
                            {
                                pedbOnFloor[instance].position = enemy.deathPosition;
                                pedbOnFloor[instance].rotation = enemy.carRotation;
                            }
                        }
                        if (enemy.color == "Red")
                        {
                            if (nextBody == 0)
                            {
                                ped2OnFloor[instance].position = enemy.deathPosition;
                                ped2OnFloor[instance].rotation = enemy.carRotation;
                            }
                            if (nextBody == 1)
                            {
                                pedb2OnFloor[instance].position = enemy.deathPosition;
                                pedb2OnFloor[instance].rotation = enemy.carRotation;
                            }
                        }
                        if (enemy.color == "Blue")
                        {
                            if (nextBody == 0)
                            {
                                ped3OnFloor[instance].position = enemy.deathPosition;
                                ped3OnFloor[instance].rotation = enemy.carRotation;
                            }
                            if (nextBody == 1)
                            {
                                pedb3OnFloor[instance].position = enemy.deathPosition;
                                pedb3OnFloor[instance].rotation = enemy.carRotation;
                            }
                        }
                        if (enemy.color == "Green")
                        {
                            if (nextBody == 0)
                            {
                                ped4OnFloor[instance].position = enemy.deathPosition;
                                ped4OnFloor[instance].rotation = enemy.carRotation;
                            }
                            if (nextBody == 1)
                            {
                                pedb4OnFloor[instance].position = enemy.deathPosition;
                                pedb4OnFloor[instance].rotation = enemy.carRotation;
                            }
                        }
                        if (enemy.color == "Purple")
                        {
                            if (nextBody == 0)
                            {
                                ped5OnFloor[instance].position = enemy.deathPosition;
                                ped5OnFloor[instance].rotation = enemy.carRotation;
                            }
                            if (nextBody == 1)
                            {
                                pedb5OnFloor[instance].position = enemy.deathPosition;
                                pedb5OnFloor[instance].rotation = enemy.carRotation;
                            }
                        }
                            if (nextBody == 0)
                                bloodOnFloor[instance].position = enemy.deathPosition;
                            if (nextBody == 1)
                                blood2OnFloor[instance].position = enemy.deathPosition;

                        instance += 1;
                        maxInstance += 1;
                        if (instance > 50)   // We need at least this number to minus it
                        {
                            destroyInstance = instance - 50;  // If 50 more instances are created they start to disapear
                            bloodOnFloor[destroyInstance].position = new Vector2(-20, -20);
                            blood2OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedOnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedbOnFloor[destroyInstance].position = new Vector2(-20, -20);
                            ped2OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedb2OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            ped3OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedb3OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            ped4OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedb4OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            ped5OnFloor[destroyInstance].position = new Vector2(-20, -20);
                            pedb5OnFloor[destroyInstance].position = new Vector2(-20, -20);
                        }
                        
                        if (instance + 2 == Blood.maxBlood) // Keeps the game from planting as there would not be enough enemies if we carried on...
                        {
                            instance = 0;
                            maxInstance = 1;
                        }
                        
                        soundBank.PlayCue("SFX_COLLISION_CAR_PED_BOUNCE");
                        enemy.alive = false;
                        enemy.isRanOver = false;
                    }
                    if(!viewportRect.Contains(new Point(
                        (int)enemy.pedBody.Position.X, (int)enemy.pedBody.Position.Y)))
                    {
                        enemy.pedBody.ResetDynamics();
                        gameStarted = true;
                        enemy.alive = false;
                    }
                }
                else
                {
                    enemy.pedBody.ResetDynamics();
                    enemy.alive = true;
                    pedTalk = sound.Next(100);
                    if (pedTalk == 20 && gameStarted == true)
                    {
                        soundBank.PlayCue("SFX_NEW_PED_DGOD3");
                    }
                    if (pedTalk == 50 && gameStarted == true)
                    {
                        soundBank.PlayCue("SFX_NEW_PED_DWATCH2");
                    }
                    if (pedTalk == 80 && gameStarted == true)
                    {
                        soundBank.PlayCue("SFX_NEW_PED_DKICK2");
                    }
                    nextPath = path.Next(300);
                    if (nextPath >= 150 && nextPath <= 200) // LEFT
                    {
                        enemy.pedBody.Rotation = 0.0f;
                        enemy.pedBody.LinearVelocity.X = MathHelper.Lerp(
                            enemy.minPedVelocity,
                            enemy.maxPedVelocity,
                            (float)random.NextDouble());
                        enemy.pedBody.Position = new Vector2(viewportRect.Left, MathHelper.Lerp(
                            (float)viewportRect.Height * Pedestrian.minEnemyHeight,
                            (float)viewportRect.Height * Pedestrian.maxEnemyHeight,
                            (float)random.NextDouble()));
                    }
                    else if (nextPath < 150) // RIGHT
                    {
                        enemy.pedBody.ResetDynamics();
                        enemy.pedBody.Rotation = 3.21f;
                        enemy.pedBody.LinearVelocity.X = -MathHelper.Lerp(
                            enemy.minPedVelocity,
                            enemy.maxPedVelocity,
                            (float)random.NextDouble());
                        enemy.pedBody.Position = new Vector2(viewportRect.Right, MathHelper.Lerp(
                            (float)viewportRect.Height * Pedestrian.minEnemyHeight,
                            (float)viewportRect.Height * Pedestrian.maxEnemyHeight,
                            (float)random.NextDouble()));
                    }
                    else if (nextPath > 200)// TOP
                    {
                        enemy.pedBody.ResetDynamics();
                        enemy.pedBody.Rotation = 1.64f;
                        enemy.pedBody.LinearVelocity.Y = MathHelper.Lerp(
                            enemy.minPedVelocity,
                            enemy.maxPedVelocity,
                            (float)random.NextDouble());
                        enemy.pedBody.Position = new Vector2(MathHelper.Lerp(
                            (float)viewportRect.Width * Pedestrian.minEnemyWidth,
                            (float)viewportRect.Width * Pedestrian.maxEnemyWidth,
                            (float)random.NextDouble()), viewportRect.Top);
                    }

                }
            }
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin( SpriteSortMode.Deferred, BlendState.AlphaBlend);
            spriteBatch.Draw(backgroundTexture, viewportRect, Color.White);


            // Draw Blood
            for (instance = 0; instance < maxInstance; instance++)
            {
                spriteBatch.Draw(bloodOnFloor[instance].bloodTexture, bloodOnFloor[instance].position, null, Color.White, bloodOnFloor[instance].rotation, bloodOnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(blood2OnFloor[instance].bloodTexture, blood2OnFloor[instance].position, null, Color.White, blood2OnFloor[instance].rotation, blood2OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                
                spriteBatch.Draw(pedOnFloor[instance].bloodTexture, pedOnFloor[instance].position, null, Color.White, pedOnFloor[instance].rotation, pedOnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(pedbOnFloor[instance].bloodTexture, pedbOnFloor[instance].position, null, Color.White, pedbOnFloor[instance].rotation, pedbOnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                
                spriteBatch.Draw(ped2OnFloor[instance].bloodTexture, ped2OnFloor[instance].position, null, Color.White, ped2OnFloor[instance].rotation, ped2OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(pedb2OnFloor[instance].bloodTexture, pedb2OnFloor[instance].position, null, Color.White, pedb2OnFloor[instance].rotation, pedb2OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                
                spriteBatch.Draw(ped3OnFloor[instance].bloodTexture, ped3OnFloor[instance].position, null, Color.White, ped3OnFloor[instance].rotation, ped3OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(pedb3OnFloor[instance].bloodTexture, pedb3OnFloor[instance].position, null, Color.White, pedb3OnFloor[instance].rotation, pedb3OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                
                spriteBatch.Draw(ped4OnFloor[instance].bloodTexture, ped4OnFloor[instance].position, null, Color.White, ped4OnFloor[instance].rotation, ped4OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(pedb4OnFloor[instance].bloodTexture, pedb4OnFloor[instance].position, null, Color.White, pedb4OnFloor[instance].rotation, pedb4OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);

                spriteBatch.Draw(ped5OnFloor[instance].bloodTexture, ped5OnFloor[instance].position, null, Color.White, ped5OnFloor[instance].rotation, ped5OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
                spriteBatch.Draw(pedb5OnFloor[instance].bloodTexture, pedb5OnFloor[instance].position, null, Color.White, pedb5OnFloor[instance].rotation, pedb5OnFloor[instance].center, 1.0f, SpriteEffects.None, 0);
          
            }
            
            // Draw Pedestrians
            foreach (Pedestrian enemy in enemies)
            {
                if (enemy.alive)
                {
                    spriteBatch.Draw(enemy.pedTexture, enemy.pedGeom.Position, null, Color.White, enemy.pedGeom.Rotation, enemy.pedOrigin, 1, SpriteEffects.None, 1);
                }
            }
            
            // Draw Borders
            //spriteBatch.Draw(right.borderTexture, right.borderGeom.Position, null, Color.White, right.borderGeom.Rotation, right.borderOrigin, 1, SpriteEffects.None, 1);
            //spriteBatch.Draw(left.borderTexture, left.borderGeom.Position, null, Color.White, left.borderGeom.Rotation, left.borderOrigin, 1, SpriteEffects.None, 1);
            //spriteBatch.Draw(top.borderTexture, left.borderGeom.Position, null, Color.White, top.borderGeom.Rotation, top.borderOrigin, 1, SpriteEffects.None, 1);
            //spriteBatch.Draw(bottom.borderTexture, bottom.borderGeom.Position, null, Color.White, bottom.borderGeom.Rotation, bottom.borderOrigin, 1, SpriteEffects.None, 1);

            // Boxes
            spriteBatch.Draw(first.boxTexture, first.boxBody.Position, null, Color.White, first.boxGeom.Rotation, first.boxOrigin, 1, SpriteEffects.None, 1);


            // Draw Players
            spriteBatch.Draw(player1.carTexture, player1.carGeom.Position, null, Color.White, player1.carGeom.Rotation, player1.carOrigin, 1, SpriteEffects.None, 1);
            spriteBatch.Draw(player2.carTexture, player2.carGeom.Position, null, Color.White, player2.carGeom.Rotation, player2.carOrigin, 1, SpriteEffects.None, 1);
            spriteBatch.Draw(player3.carTexture, player3.carGeom.Position, null, Color.White, player3.carGeom.Rotation, player3.carOrigin, 1, SpriteEffects.None, 1);
            spriteBatch.Draw(player4.carTexture, player4.carGeom.Position, null, Color.White, player4.carGeom.Rotation, player4.carOrigin, 1, SpriteEffects.None, 1);

            
            // Draw Scores
            spriteBatch.DrawString(ScoreFont, player1.giveScore, player1.positionScore, Color.White);
            spriteBatch.DrawString(ScoreFont, player2.giveScore, player2.positionScore, Color.White);
            spriteBatch.DrawString(ScoreFont, player3.giveScore, player3.positionScore, Color.White);
            spriteBatch.DrawString(ScoreFont, player4.giveScore, player4.positionScore, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
