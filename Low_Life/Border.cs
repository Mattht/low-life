﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace Low_Life
{
    class Border
    {
        public Body borderBody;
        public Geom borderGeom;
        public Texture2D borderTexture;
        public Vector2 borderOrigin;

        public Border(Texture2D loadedtexture)
        {
            borderTexture = loadedtexture;
            
                borderBody = BodyFactory.Instance.CreateRectangleBody(600, 20, 1);
                borderOrigin = new Vector2(10, 300);

                borderGeom = GeomFactory.Instance.CreateRectangleGeom(this.borderBody, this.borderTexture.Width, this.borderTexture.Height);
                borderGeom.CollisionGroup = 1; // Same as Peds
                borderGeom.RestitutionCoefficient = 0.0f;
                borderGeom.FrictionCoefficient = 0.0f;
                borderGeom.Tag = this;

                borderBody.IsStatic = true;
        }
    }
}
